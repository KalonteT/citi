public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = {
            new SavingsAccount(2,"Killua"),new CurrentAccount(4,"Gon"), new SavingsAccount(6,"Leorio")
        };
        for (Account account : accounts) {
            account.addInterest();
            System.out.println(account.getName()+"'s Balance: "+account.getBalance());
        }
        Detailables[] deets = {
            new SavingsAccount(10000, "Kali"),
            new CurrentAccount(5000, "Magnolia"),
            new HomeInsurance(700, 100, 800)
        };
        for (Detailables detailables : deets) {
            System.out.println(detailables.getDetails());
        }
    }
}