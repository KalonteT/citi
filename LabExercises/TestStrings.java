public class TestStrings {
    public static void main(String[] args) {
        String str1= new String();
        String str2 = new String();
        str1 = "example.doc";
        str2 = "example.doc";
        //System.out.println(str1);
        if(str1==str2)
            System.out.println("The strings are equal");
        else{
        // This line compares str1 to str2 and returns the string the furthest aong lexigraphocally
        String lexResult = ((str1.compareTo(str2)>0)? str1 : str2);
        System.out.println(lexResult+" is further along in the dictionary lexigraphocally");
        }

        String owString = new String("the quick brown fox swallowed down the lazy chicken");
        int owCounter = 0;
        for (int i = 0, n = owString.length(); i < n; i++){
            char c = owString.charAt(i);
            if(c =='w'&&i!=0){
                char checkO = owString.charAt(i-1);
                if(checkO == 'o'){
                    owCounter++; 
                }        
            }
        }
        System.out.println("There were "+owCounter+" 'ow' strings in the input string");

        String palindromeStr = new String("Live not on evil");
        palindromeStr = palindromeStr.replaceAll("\\s", "");
        //System.out.println(palindromeStr);
        StringBuilder inputPalindrome = new StringBuilder();
        inputPalindrome.append(palindromeStr);
        String palindromeStr2 = new String(inputPalindrome.reverse().toString());
        String palindromeResult = ((palindromeStr.equalsIgnoreCase(palindromeStr2))? "This string is a palindrome" : "This string is not a palindrome");
        System.out.println(palindromeResult);
        String date = "08/25/2020";
        String date2 = "26.08.2020";
        System.out.println(date);
        System.out.println(date2);
        String loremIpsum = "Lorem ipsum dolor sit amet, eius velit dissentias et ius, regione necessitatibus id nam. In ludus deleniti qui. Modo aliquip vix cu, ut dicam fierent persequeris est. Ut est graece fabulas disputando, quo aliquam labores voluptua ex. Quaeque percipit voluptaria at has, velit fierent no vim.";
        int sentenceCount = 0;
        
        String[] str_array = loremIpsum.split("[.]");
        for (String o : str_array) {
            int wordCount= 0;
            o = o.concat(".");
            //System.out.println(o);
            sentenceCount++;
            String[] wordArray = o.split(" ");
            for (String word : wordArray) {
                if(word.compareTo("")!=0)
                    wordCount++;
                //System.out.println("Added word");
                //System.out.println(word);
            }
            System.out.println("Word Count for Sentence: " + wordCount);
        }
        System.out.println("Sentence Count: " + sentenceCount);
    }
}