public class TestAccount2 {

    public static void main(String[] args) {
        Account[] arrayAcc = new Account[5];
        for(int i = 0; i < 5; i++){
            arrayAcc[i] = new Account((double)(i*100),"Test Account: "+i);
        }

        for (Account account : arrayAcc) {
            System.out.println(account.getName());
            System.out.println(account.getBalance());
        }
        Account.setInterestRate(2.1);
        for (Account account : arrayAcc) {
            System.out.println(account.getName()+"'s balance before is "+account.getBalance());
            account.addInterest();
            System.out.println(account.getName()+"'s balance after is "+account.getBalance());
            account.withdraw();
            System.out.println(account.getName()+"'s balance after is "+account.getBalance());
            account.withdraw(200);
            System.out.println(account.getName()+"'s balance after is "+account.getBalance());
        }
    }
    
}