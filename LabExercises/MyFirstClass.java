public class MyFirstClass{
    public static void main(String[] args){
        String make = "BMW";
        String model = "330i";
        double engineSize=4.0;
        byte gear = 3;
        short speed = (short)(gear*20);

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);
        System.out.println("The engine size is " + speed);
        System.out.println("Hello from my first java program");
        if(engineSize>1.3){
            System.out.println("This car is very powerful");
        }
        if(gear==3){
            System.out.println("Speed should be 60 mph");
        }
        else{
            System.out.println("Speed should be what you want it to be");
        }
        switch(gear){
            case 3:
                System.out.println("Speed should be 60mph");
                break;
            case 2:
            System.out.println("Speed should be 60mph");
            break;

            default: 
            System.out.println("Speed is nothing");
            break;
        }
        int counter = 0;
        for(int i = 1900; i<2001;i++){
            if(i%4==0){
                System.out.println("This is a leap year " + i);
                counter++;
            }
            if(counter>4)
                break;
        }
    }
}