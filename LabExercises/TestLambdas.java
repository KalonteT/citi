public class TestLambdas {
    public static void main(String[] args) {
        Detailables det = () -> {return "hello world";};

        System.out.println(det.getDetails());
    }
    
}